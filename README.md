# Orange
The orange package provides recommendations on how to align a python package's directory tree closer with its internal dependency graph.

## Installation
1. Clone this repo:
```bash
git clone git@gitlab.com:jdcoding/orange.git
```
2. (Optional) Create virtual environment to keep the python environment separated from other python applications, e.g. using [conda](https://www.anaconda.com/):
```bash
cd orange
conda create -n orange python=3.8
conda activate orange
```
3. Install orange
```
pip install .
```


## Usage
Currently, orange supports two commands. In both cases, orange analyzes a python package or a subfolder of it for its internal dependency structure.
### Plotting a dependency graph of a python (sub)package
```
orange plot-dependencies <path of package>
```
Creates a plot of the internal dependencies of the package in the current working directory.

### Checking if a (sub)package directory tree matches orange's suggestion for its dependency graph
This command is intended for the use within CI pipelines.
```
orange check <path of package>
```
Calculates a suggested directory tree for the package and returns 0 if the suggestion matches the existing directory tree. If it does not match, orange prints the suggested directory tree and returns 1.
