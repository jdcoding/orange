import os
import subprocess

from networkx import DiGraph


def package_name_from_package_path(package_path):
    package_name = os.path.split(package_path)[1]
    return package_name


def parse_file_tree_to_dependency_dict(test_package_path):
    package_name = package_name_from_package_path(test_package_path)
    deps = subprocess.run(
        f"pydeps  --only {package_name} --no-output "
        f"--show-raw-deps {test_package_path}",
        capture_output=True,
        shell=True,
    )
    dep_dict = eval(deps.stdout.replace(b"null", b"None"))
    dep_dict = {
        key: dep_dict[key] for key in dep_dict.keys() if key.startswith(package_name)
    }
    dep_dict = remove_dunder_files(dep_dict)
    return dep_dict


def dependency_dict_to_graph(dep_dict):
    dep_graph = DiGraph()
    for node, properties in dep_dict.items():
        dep_graph.add_node(node)
        if "imported_by" in properties:
            for imported_by in properties["imported_by"]:
                if imported_by != "__main__":
                    dep_graph.add_edge(imported_by, node)
    return dep_graph


def remove_dunder_files(dep_dict: dict) -> dict:
    keys = list(dep_dict.keys())
    for key in keys:
        path = dep_dict[key]["path"]
        if "__init__.py" in path:
            dep_dict.pop(key)
    return dep_dict
