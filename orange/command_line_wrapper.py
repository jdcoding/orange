import os

import click
from networkx.drawing.nx_pydot import write_dot as write_dot_nx

from orange.directory_suggestion.io import (
    suggested_file_tree_from_package_path,
    suggested_file_tree_matches_package_path,
    file_suffix,
    print_file_tree,
    plot_dependency_graph,
)
from orange.dep_dict_generation import (
    package_name_from_package_path,
    parse_file_tree_to_dependency_dict,
    dependency_dict_to_graph,
)


@click.group()
def click_main():
    """
    Orange: I can provide recommendations on how to align a projects directory tree
    with its internal dependency graph.

    orange COMMAND OPTIONS
    """
    pass


@click_main.command()
@click.argument("package_path")
def check(package_path):
    """
    Orange: I will check if the directory tree matches of PACKAGE_PATH matches my
    suggestion for its  internal dependency graph.
    :param package_path: The path of the directory I am supposed to check.
    :return: 0 if the diretory tree matches the dependency graph, else 1.
    """
    tree = suggested_file_tree_from_package_path(package_path)
    if suggested_file_tree_matches_package_path(tree, package_path, file_suffix):
        print(
            f"Orange: Your folder structure for package {package_path} matches my "
            f"suggestion."
        )
        return 0
    else:
        print("Orange: I suggest the following file tree:")
        print_file_tree(tree)
        return 1


@click_main.command()
@click.argument("package_path")
def write_dot(package_path):
    """
    Orange: I will analyze the dependencies of PACKAGE_PATH and writes the dependency
    graph to a dot file.
    :param package_path: The directory I am supposed to analyze.
    :return:
    """
    dep_dict = parse_file_tree_to_dependency_dict(package_path)
    dep_graph = dependency_dict_to_graph(dep_dict)
    package_name = package_name_from_package_path(package_path)
    write_dot_nx(dep_graph, package_name + ".dot")


@click_main.command()
@click.argument("package_path")
def plot_dependencies(package_path):
    """
    Orange: I will plot the internal dependency graph of a PACKAGE_PATH into the current
    working directory.


    :param package_path: The directory I am supposed to analyze.
    :return: None
    """
    file_path = plot_dependency_graph(package_path, os.getcwd())
    print(
        f"Orange: I plotted the dependency structure of '{package_path}' in the "
        f"file '{file_path}'"
    )
