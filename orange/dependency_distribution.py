from dataclasses import dataclass
from typing import Tuple

import numpy as np
from networkx import DiGraph
from pandas import DataFrame
import plotly.express as px
import plotly.graph_objects as go


@dataclass
class Annotation(object):
    text: str
    xy: Tuple[int, int]


def get_main_directory(path: str, separator: str):
    parts = str.split(path, separator)
    if "." == separator:
        parts = parts[:-1] + [parts[-1] + ".py"]
    return parts[1]


def plot_dependency_distribution(dep_graph: DiGraph, path_separator: str) -> go.Figure:
    df = get_dataframe(dep_graph, path_separator)
    marginal_mode = "rug"
    fig = px.scatter(
        df,
        x="No. files depending on this",
        y="No. of files this depends on",
        color="Package/Module",
        hover_data=["File"],
        marginal_x=marginal_mode,
        marginal_y=marginal_mode,
        title=str.split(list(dep_graph.nodes())[0], path_separator)[0],
    )
    add_annotations(fig)
    return fig


def get_dataframe(dep_graph: DiGraph, path_separator: str):
    xs = []
    ys = []
    cs = []
    for node in dep_graph.nodes():
        num_dependants = len(list(dep_graph.successors(node)))
        xs.append(num_dependants + np.random.uniform(0, 0.2))
        num_dependencies = len(list(dep_graph.predecessors(node)))
        ys.append(num_dependencies + np.random.uniform(0, 0.2))
        cs.append(get_main_directory(node, path_separator))
    df = DataFrame(
        {
            "No. files depending on this": xs,
            "No. of files this depends on": ys,
            "Package/Module": cs,
            "File": dep_graph.nodes(),
        }
    )

    return df


def add_annotations(fig):
    annotations = [
        Annotation(text="Libraries", xy=(1, 0)),
        Annotation(text="Core", xy=(1, 1)),
        Annotation(text="Use cases", xy=(0, 1)),
        Annotation(text="Utils", xy=(0, 0)),
    ]
    for a in annotations:
        fig.add_annotation(
            xref="x domain",
            yref="y domain",
            x=compute_xy(a.xy[0]),
            y=compute_xy(a.xy[1]),
            text=a.text,
            showarrow=False,
        )


def compute_xy(xy: float) -> float:
    padding = 0.1
    axis_center = 0.5
    return (xy - axis_center) * (1 - padding) + axis_center


def python_figure(dep_graph: DiGraph, separator: str):
    fig = plot_dependency_distribution(dep_graph, separator)
    fig.show()
