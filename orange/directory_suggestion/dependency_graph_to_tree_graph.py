from copy import deepcopy
from dataclasses import dataclass

from networkx import DiGraph, contracted_nodes, cycle_basis, Graph


def dependency_graph_to_tree_graph(graph, root_name):
    graph = _merge_non_branching_nodes(graph)
    graph = _merge_cycles(graph)
    graph = _merge_single_file_leaves(graph)
    graph = _tree_to_folder_graph(graph, root_name)
    return graph


def _merge_non_branching_nodes(graph: DiGraph):
    merged_graph = deepcopy(graph)
    edges = list(merged_graph.edges)
    while edges:
        edge = edges.pop()
        if all(
            [
                len(list(neighbors(edge[index]))) < 2
                for neighbors in [graph.predecessors, graph.successors]
                for index in [0, 1]
            ]
        ):
            node_to_be_contracted_into = edge[0]
            contracted_node = edge[1]
            merged_graph = contracted_nodes(
                merged_graph,
                node_to_be_contracted_into,
                contracted_node,
                self_loops=False,
            )
            edges = replace_string(edges, contracted_node, node_to_be_contracted_into)
    return merged_graph


def replace_string(list_of_tuples, node_to_be_replaced, replace_by):
    return list(
        [
            tuple(replace_by if node == node_to_be_replaced else node for node in e)
            for e in list_of_tuples
        ]
    )


def _merge_cycles(graph):
    folded_graph = deepcopy(graph)
    edge_chains = _calculate_edge_chains_in_cycles(graph)

    while edge_chains:
        folded_graph, edge_chains = _merge_first_chain(folded_graph, edge_chains)
    return folded_graph


def _calculate_edge_chains_in_cycles(graph):
    edge_chains = []
    for cycle in cycle_basis(Graph(graph)):
        imported_nodes, importing_nodes = _get_cycle_importing_imported_nodes(
            graph, cycle
        )
        for importing_node in importing_nodes:
            for step_ix in [1, -1]:
                edge_chain = _calculate_edge_chain(
                    cycle, imported_nodes, importing_node, step_ix
                )
                edge_chains = _update_edge_chains(edge_chain, edge_chains)
    return edge_chains


def _calculate_edge_chain(cycle, imported_nodes, importing_node, step_ix):
    ix = cycle.index(importing_node)
    node = cycle[ix]
    edge_chain = [node]
    while cycle[ix] not in imported_nodes:
        ix = (ix + step_ix) % len(cycle)
        edge_chain.append(cycle[ix])
    return edge_chain


def _update_edge_chains(edge_chain, edge_chains):
    edge_chains_updated = deepcopy(edge_chains)
    if edge_chain not in edge_chains:
        edge_chains_updated.append(edge_chain)
    return edge_chains_updated


def _get_cycle_importing_imported_nodes(graph, cycle):
    importing_nodes = []
    imported_nodes = []
    for node in cycle:
        successors = list(graph.successors(node))
        predecessors = list(graph.predecessors(node))
        if len(list(filter(lambda x: x in cycle, successors))) == 2:
            importing_nodes.append(node)
        elif len(list(filter(lambda x: x in cycle, predecessors))) == 2:
            imported_nodes.append(node)
    return imported_nodes, importing_nodes


def _merge_first_chain(graph, edge_chains):
    merged_graph = deepcopy(graph)
    edge_chain = edge_chains.pop()
    for ix in range(len(edge_chain) // 2):
        a, b = edge_chain[ix], edge_chain[-1 - ix]
        if a != b:
            merged_graph = contracted_nodes(graph, a, b, self_loops=False)
            edge_chains_with_single_nodes = [
                _replace_nodes(edge_chain, replace=b, by=a)
                for edge_chain in edge_chains
            ]
            edge_chains = list(
                filter(lambda x: len(x) > 1, edge_chains_with_single_nodes)
            )
            for bidirectional_edge in _intersection(
                list(merged_graph.predecessors(a)), list(merged_graph.successors(a))
            ):
                merged_graph.remove_edge(bidirectional_edge, a)
    return merged_graph, edge_chains


def _replace_nodes(edge_chain, replace, by):
    res = []
    for node in edge_chain:
        if replace == node:
            to_append = by
        else:
            to_append = node
        if not res or res[-1] != to_append:
            res.append(to_append)
    return res


def _intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3


@dataclass
class _MergeSingleFileLeavesParams:
    to_be_merged_ix: int
    to_be_merged_into_ix: int
    num_successors_of_node_to_be_merged: int
    num_predecessors_of_node_to_be_merged: int


def _merge_single_file_leaves(graph: DiGraph) -> DiGraph:
    merged_graph = deepcopy(graph)

    right_edges_params = _MergeSingleFileLeavesParams(
        to_be_merged_ix=1,
        to_be_merged_into_ix=0,
        num_successors_of_node_to_be_merged=0,
        num_predecessors_of_node_to_be_merged=1,
    )
    merged_graph = _merge_single_file_nodes_one_direction(
        merged_graph, right_edges_params
    )
    left_edges_params = _MergeSingleFileLeavesParams(
        to_be_merged_ix=0,
        to_be_merged_into_ix=1,
        num_successors_of_node_to_be_merged=1,
        num_predecessors_of_node_to_be_merged=0,
    )
    merged_graph = _merge_single_file_nodes_one_direction(
        merged_graph, left_edges_params
    )
    return merged_graph


def _merge_single_file_nodes_one_direction(
    merged_graph, params: _MergeSingleFileLeavesParams
):
    merged_graph = deepcopy(merged_graph)
    right_edges_to_be_merged = [
        e
        for e in merged_graph.edges()
        if len(list(merged_graph.successors(e[params.to_be_merged_ix])))
        == params.num_successors_of_node_to_be_merged
        and len(list(merged_graph.predecessors(e[params.to_be_merged_ix])))
        == params.num_predecessors_of_node_to_be_merged
        and "contraction" not in merged_graph.nodes[e[params.to_be_merged_ix]].keys()
    ]
    for edge in right_edges_to_be_merged:
        merged_graph = contracted_nodes(
            merged_graph,
            edge[params.to_be_merged_into_ix],
            edge[params.to_be_merged_ix],
            self_loops=False,
        )
    return merged_graph


def _tree_to_folder_graph(graph: DiGraph, root_name: str) -> DiGraph:
    folder_graph = DiGraph()
    graph_roots = list(
        filter(lambda x: len(list(graph.predecessors(x))) == 0, graph.nodes())
    )
    for graph_root in graph_roots:
        folder_graph = _add_to_tree(folder_graph, graph, graph_root, parent=root_name)
    return folder_graph


def _add_to_tree(tree: DiGraph, source_graph: DiGraph, node: str, parent: str):
    tree.add_edge(parent, node)
    contracted_nodes = _get_contracted_nodes_recursively(source_graph.nodes[node])
    for contracted_node in contracted_nodes:
        tree.add_edge(parent, contracted_node)
    for child in source_graph.successors(node):
        new_directory_name = child + "_dir"
        tree.add_edge(parent, new_directory_name)
        tree = _add_to_tree(tree, source_graph, child, parent=new_directory_name)
    return tree


def _get_contracted_nodes_recursively(contraction_dict: dict):
    ret = []
    if "contraction" in contraction_dict.keys():
        for k, v in contraction_dict["contraction"].items():
            ret.append(k)
            ret.extend(_get_contracted_nodes_recursively(v))
    return ret
