import os

import matplotlib.pyplot as plt
from anytree import Node, RenderTree
from networkx import DiGraph, is_isomorphic, draw_circular

from orange.dep_dict_generation import (
    parse_file_tree_to_dependency_dict,
    dependency_dict_to_graph,
)
from orange.directory_suggestion.dependency_graph_to_tree_graph import (
    dependency_graph_to_tree_graph,
)

file_suffix = ".py"


def suggested_file_tree_from_package_path(test_package_path: str) -> DiGraph:
    root_name = os.path.split(test_package_path)[-1]
    dep_dict = parse_file_tree_to_dependency_dict(test_package_path)
    dependency_graph = dependency_dict_to_graph(dep_dict=dep_dict)
    graph = dependency_graph_to_tree_graph(dependency_graph, root_name)
    return graph


def _get_files_from_directory_recursively(
    test_package_path: str, file_suffix: str
) -> DiGraph:
    test_package_depth = len(test_package_path.split(os.path.sep))
    file_tree = DiGraph()
    for root, dirs, files in os.walk(test_package_path):
        parent = os.path.join(*root.split(os.path.sep)[test_package_depth - 1 :])
        for f in files:
            if f.endswith(file_suffix) and f != "__init__.py":
                file_tree.add_edge(
                    parent,
                    os.path.join(parent, f),
                )
    _add_directories(file_tree, test_package_depth, test_package_path)
    return file_tree


def _add_directories(file_tree, test_package_depth, test_package_path):
    all_dirs_added = False
    while not all_dirs_added:
        all_dirs_added = True
        nodes = file_tree.nodes()
        for root, dirs, _ in os.walk(test_package_path):
            parent = os.path.join(*root.split(os.path.sep)[test_package_depth - 1 :])
            for d in dirs:
                dir_path = os.path.join(parent, d)
                if dir_path in nodes and (parent, dir_path) not in file_tree.edges():
                    file_tree.add_edge(parent, dir_path)
                    all_dirs_added = False


def suggested_file_tree_matches_package_path(
    file_tree: DiGraph, test_package_path: str, file_suffix: str
) -> bool:
    true_file_tree = _get_files_from_directory_recursively(
        test_package_path, file_suffix
    )
    return is_isomorphic(true_file_tree, file_tree)


def print_file_tree(file_tree: DiGraph):
    anytree_nodes = {}
    for node in file_tree.nodes():
        anytree_nodes[node] = Node(os.path.split(node)[1])
    for edge in file_tree.edges():
        anytree_nodes[edge[1]].parent = anytree_nodes[edge[0]]
    root = list(filter(lambda x: x.parent is None, anytree_nodes.values()))[0]
    for pre, _, node in RenderTree(root):
        print(f"{pre}, {node.name}")


def plot_dependency_graph(test_package_path, dir):
    dependency_dict = parse_file_tree_to_dependency_dict(test_package_path)
    dependency_graph = dependency_dict_to_graph(dependency_dict)
    plt.figure()
    draw_circular(dependency_graph, with_labels=True)
    file_path = os.path.join(dir, os.path.split(test_package_path)[1]) + ".svg"
    plt.savefig(file_path)
    return file_path
