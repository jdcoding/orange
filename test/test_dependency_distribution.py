import os

import pytest
from networkx import DiGraph  # type: ignore
from networkx.drawing.nx_pydot import read_dot  # type: ignore

from orange.dependency_distribution import python_figure


@pytest.fixture
def current_folder():
    return os.path.split(__file__)[0]


def test_plot():
    dep_graph = DiGraph()
    dep_graph.add_edge("a/a", "b/a")
    dep_graph.add_edge("a/a", "c/a")
    dep_graph.add_edge("a/a", "d/a")
    dep_graph.add_edge("b/a", "d/a")
    dep_graph.add_edge("b/b", "d/b")
    dep_graph.add_edge("b/c", "a/b")
    python_figure(dep_graph, "/")


def test_plot_youtube_dl(current_folder):
    dep_graph = read_dot(os.path.join(current_folder, "youtube_dl.dot"))
    dep_graph.remove_node("\\n")
    python_figure(dep_graph, ".")


def test_plot_the_fuck(current_folder):
    dep_graph = read_dot(os.path.join(current_folder, "thefuck.dot"))
    dep_graph.remove_node("\\n")
    python_figure(dep_graph, ".")


def test_plot_django(current_folder):
    dep_graph = read_dot(os.path.join(current_folder, "./django.dot"))
    dep_graph.remove_node("\\n")
    python_figure(dep_graph, ".")
