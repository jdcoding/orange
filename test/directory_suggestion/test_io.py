import os
import tempfile

import pytest
from networkx import DiGraph, is_isomorphic  # type: ignore

from orange.directory_suggestion.io import (
    suggested_file_tree_from_package_path,
    suggested_file_tree_matches_package_path,
    file_suffix,
    print_file_tree,
    plot_dependency_graph,
    parse_file_tree_to_dependency_dict,
)


@pytest.fixture
def current_folder():
    return os.path.split(__file__)[0]


def test_single_file(current_folder):
    test_package_path = os.path.join(
        current_folder, "test_packages", "single_file_package"
    )
    file_tree = suggested_file_tree_from_package_path(test_package_path)
    expected_tree = DiGraph()
    expected_tree.add_edge("single_file_package", "a")
    assert is_isomorphic(expected_tree, file_tree)


def test_suggested_file_tree_matches_package(current_folder):
    test_package_path = os.path.join(
        current_folder, "test_packages", "single_file_package"
    )
    file_tree = suggested_file_tree_from_package_path(test_package_path)
    assert suggested_file_tree_matches_package_path(
        file_tree, test_package_path, file_suffix
    )


def test_orange_package_matches_suggested_file_tree(current_folder):
    test_package_path = os.path.join(current_folder, "../..", "orange")
    file_tree = suggested_file_tree_from_package_path(test_package_path)
    assert suggested_file_tree_matches_package_path(
        file_tree, test_package_path, file_suffix
    )


def test_single_folder_package(current_folder):
    test_package_path = os.path.join(
        current_folder, "test_packages", "single_folder_package"
    )
    file_tree = suggested_file_tree_from_package_path(test_package_path)
    expected_tree = DiGraph()
    expected_tree.add_edge("single_folder_package", "a")
    expected_tree.add_edge("single_folder_package", "d")
    expected_tree.add_edge("single_folder_package", "a_dir")
    expected_tree.add_edge("a_dir", "b")
    expected_tree.add_edge("a_dir", "c")
    print_file_tree(file_tree)
    assert is_isomorphic(expected_tree, file_tree)


def test_print_tree(current_folder):
    test_package_path = os.path.join(
        current_folder, "test_packages", "single_file_package"
    )
    file_tree = suggested_file_tree_from_package_path(test_package_path)
    print_file_tree(file_tree)


def test_plot_dependency_graph(current_folder):
    test_package_path = os.path.join(
        current_folder, "test_packages", "single_folder_package"
    )
    with tempfile.TemporaryDirectory() as dir:
        plot_dependency_graph(test_package_path, dir)
        assert len(os.listdir(dir)) > 0


def test_parse_file_tree_to_dependency_dict_filters_inits(current_folder):
    # using the orange package since it has an __init__ file
    test_package_path = os.path.join(current_folder, "../..", "orange")
    dep_dict = parse_file_tree_to_dependency_dict(test_package_path)
    for val in dep_dict.values():
        assert not val["path"].endswith("__init__.py")
