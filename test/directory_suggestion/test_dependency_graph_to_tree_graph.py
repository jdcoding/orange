from networkx import DiGraph, is_isomorphic  # type: ignore

from orange.directory_suggestion.dependency_graph_to_tree_graph import (
    _add_to_tree,
    _get_contracted_nodes_recursively,
    _merge_non_branching_nodes,
    _merge_first_chain,
    _merge_cycles,
    dependency_graph_to_tree_graph,
    _tree_to_folder_graph,
)


def test_add_to_tree_recursive_contractions():
    tree = DiGraph()
    tree.add_edge("root", "a")
    source_graph = DiGraph()
    source_graph.add_node("a", contraction={"b": {"contraction": {"c": {}}}})
    new_tree = _add_to_tree(tree, source_graph, "a", "root")
    expected_tree = DiGraph()
    expected_tree.add_edge("root", "a")
    expected_tree.add_edge("root", "b")
    expected_tree.add_edge("root", "c")
    assert is_isomorphic(new_tree, expected_tree)


def test_get_contracted_nodes_recursively():
    assert ["b", "c"] == _get_contracted_nodes_recursively(
        {"contraction": {"b": {"contraction": {"c": {}}}}}
    )


def test_merge_non_branching_nodes_chain_with_three_nodes():
    dependency_graph = DiGraph()
    dependency_graph.add_edge("a", "b")
    dependency_graph.add_edge("b", "c")
    suggested_graph = _merge_non_branching_nodes(dependency_graph)
    expected_graph = DiGraph()
    expected_graph.add_node("a")
    assert is_isomorphic(expected_graph, suggested_graph)


def test_merge_first_chain_colliding_circles():
    dependency_graph = DiGraph()
    dependency_graph.add_edge("a", "c")
    dependency_graph.add_edge("b", "c")
    dependency_graph.add_edge("a", "d")
    dependency_graph.add_edge("b", "d")
    dependency_graph.add_edge("c", "d")
    suggested_graph, _ = _merge_first_chain(dependency_graph, [["a", "d"]])
    expected_tree = DiGraph()
    expected_tree.add_edge("a", "c")
    expected_tree.add_edge("b", "c")
    expected_tree.add_edge("b", "a")
    assert is_isomorphic(expected_tree, suggested_graph)


def test_merge_first_chain_self_contraction():
    dependency_graph = DiGraph()
    dependency_graph.add_edge("a", "c")
    dependency_graph.add_edge("c", "a")
    suggested_graph, _ = _merge_first_chain(dependency_graph, [["a", "c", "a"]])
    assert "contraction" not in suggested_graph.nodes["a"]


def test_merge_cycles_additional_out_edge_is_not_inverted():
    dependency_graph = DiGraph()
    dependency_graph.add_edge("a", "b")
    dependency_graph.add_edge("a", "c")
    dependency_graph.add_edge("b", "d")
    dependency_graph.add_edge("c", "d")
    dependency_graph.add_edge("d", "e")
    suggested_graph = _merge_cycles(dependency_graph)
    expected_tree = DiGraph()
    expected_tree.add_edge("a", "b")
    expected_tree.add_edge("a", "c")
    expected_tree.add_edge("a", "e")
    assert is_isomorphic(expected_tree, suggested_graph)


def test_merge_cycles_additional_in_edge_is_not_inverted():
    dependency_graph = DiGraph()
    dependency_graph.add_edge("a", "b")
    dependency_graph.add_edge("a", "c")
    dependency_graph.add_edge("b", "d")
    dependency_graph.add_edge("c", "d")
    dependency_graph.add_edge("e", "d")
    suggested_graph = _merge_cycles(dependency_graph)
    expected_tree = DiGraph()
    expected_tree.add_edge("a", "b")
    expected_tree.add_edge("a", "c")
    expected_tree.add_edge("e", "a")
    assert is_isomorphic(expected_tree, suggested_graph)


def test_two_modules_importing_a_third():
    dependency_graph = DiGraph()
    dependency_graph.add_edge("a", "b")
    dependency_graph.add_edge("c", "b")
    suggested_graph = dependency_graph_to_tree_graph(dependency_graph, "root")
    expected_tree = DiGraph()
    expected_tree.add_edge("root", "a")
    expected_tree.add_edge("root", "b")
    expected_tree.add_edge("root", "c")
    assert is_isomorphic(expected_tree, suggested_graph)


def test_double_loop():
    dependency_graph = DiGraph()
    dependency_graph.add_edge("a", "b")
    dependency_graph.add_edge("a", "c")
    dependency_graph.add_edge("b", "c")
    dependency_graph.add_edge("a", "d")
    dependency_graph.add_edge("d", "c")
    suggested_graph = _merge_cycles(dependency_graph)
    expected_tree = DiGraph()
    expected_tree.add_edge("a", "b")
    expected_tree.add_edge("a", "d")
    assert is_isomorphic(expected_tree, suggested_graph)


def test_two_colliding_circles():
    dependency_graph = DiGraph()
    dependency_graph.add_edge("a", "c")
    dependency_graph.add_edge("b", "c")
    dependency_graph.add_edge("a", "d")
    dependency_graph.add_edge("b", "d")
    dependency_graph.add_edge("c", "d")
    suggested_graph = _merge_cycles(dependency_graph)
    expected_tree = DiGraph()
    expected_tree.add_edge("a", "c")
    assert is_isomorphic(expected_tree, suggested_graph)


def test_cycle_resolution_merge_cycles():
    dependency_graph = DiGraph()
    dependency_graph.add_edge("a", "b")
    dependency_graph.add_edge("a", "c")
    dependency_graph.add_edge("b", "d")
    dependency_graph.add_edge("c", "d")
    suggested_graph = _merge_cycles(dependency_graph)
    expected_tree = DiGraph()
    expected_tree.add_edge("a", "b")
    expected_tree.add_edge("a", "c")
    assert is_isomorphic(expected_tree, suggested_graph)


def test_two_independent_nodes():
    dependency_graph = DiGraph()
    dependency_graph.add_node("a")
    dependency_graph.add_node("b")
    suggested_graph = _tree_to_folder_graph(dependency_graph, "root")
    expected_graph = DiGraph()
    expected_graph.add_edge("root", "a")
    expected_graph.add_edge("root", "b")
    assert is_isomorphic(expected_graph, suggested_graph)
